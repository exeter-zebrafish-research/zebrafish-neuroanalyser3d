from setuptools import setup, Command

# Coverage command
import coverage
import unittest
import os
from concurrent.futures import (
    ThreadPoolExecutor,
    ProcessPoolExecutor,
)

PWD = os.path.abspath(os.path.dirname(__file__))
MODNAME = "zebrafish_neuroanalyser"

class Runner():

    def parallel_execution(self, *name, options='by_module'):

        """
        name - name of the class with tests or module with classes that contain tests
        modules - name of the module with tests or with class that contains tests
        options:
            by_method - gather all tests methods in the class/classes and execute in parallel
            by_module - gather all tests from modules in parallel
            by_class - will execute all classes (with tests) in parallel
        """

        suite = unittest.TestSuite()

        if (options=='by_method'):
            for object in name:
                for method in dir(object):
                    if (method.startswith('test')):
                        suite.addTest(object(method))
        elif (options=='by_class'):
            for object in name:
                suite.addTest(unittest.TestLoader().loadTestsFromTestCase(object))

        elif (options=='by_module'):
            for module in name:
                suite.addTest(unittest.TestLoader().loadTestsFromModule(module))
        else:
            raise ValueError("Parameter 'options' is incorrect."
                             "Available options: 'by_method', 'by_class', 'by_module'")

        #with ThreadPoolExecutor(max_workers=10) as executor:
        with ProcessPoolExecutor(max_workers=10) as executor:
            list_of_suites = list(suite)
            for test in range(len(list_of_suites)):
                test_name = str(list_of_suites[test])
                executor.submit(unittest.TextTestRunner().run, list_of_suites[test])


class TestCommandParallel(Command):
    """Test Command"""
    description = "Run tests in parallel execution"
    user_options = []
    def initialize_options(self):
        """init options"""
        pass
    def finalize_options(self):
        """finalize options"""
        pass
    def run(self):
        """runner"""
        # Run normal tests
        loader = unittest.TestLoader()
        suite = loader.discover('.')
        print("Discovered %d tests..."%suite.countTestCases(), flush=True)

        # Basic
        #runner = unittest.runner.TextTestRunner()
        #runner.run(suite)

        # Using other peoples modules
        #par_suite = testtools.ConcurrentStreamTestSuite(lambda: ((case, None) for case in suite))
        #par_suite.run(testtools.StreamResult())
        #par_suite = ConcurrentTestSuite(suite, fork_for_tests(7))

        # SO answer
        runner = Runner()
        tests = []
        for s in suite:
            for ss in s:
                tests.extend(ss._tests)
        runner.parallel_execution(*tests)

class CoverageCommand(Command):
    """Coverage Command"""
    description = "Run coverage on tests"
    user_options = []
    def initialize_options(self):
        """init options"""
        pass
    def finalize_options(self):
        """finalize options"""
        pass
    def run(self):
        """runner"""

        omitfiles = [
            os.path.join(MODNAME, "tests", "*"),
            os.path.join(MODNAME, "__main__.py"),
        ]
        for r, dirs, files in os.walk(MODNAME):
            omitfiles.extend(
                os.path.join(r, f) for f in
                filter(lambda f: f == "__init__.py", files)
            )

        cov = coverage.Coverage(
            source=[MODNAME],
            omit=omitfiles,
            )
        cov.start()
        # Run normal tests
        loader = unittest.TestLoader()
        suite = loader.discover('.')
        #parallel_suite = testtools.ConcurrentStreamTestSuite(lambda: ((case, None) for case in suite))
        #parallel_suite.run(testtools.StreamResult())
        runner = unittest.runner.TextTestRunner()
        runner.run(suite)
        cov.save()
        cov.html_report()
        # Alternative lazy way: call coverage in subprocess
        # subprocess....

setup(name=MODNAME,
    version='0.1',
    description='Analyses 4d zebrafish neuroimaging time-series data',
    author='Jeremy Metz',
    author_email='metz.jp@gmail.com',
    url='',
    packages=[MODNAME],
    test_suite="%s.tests"%MODNAME,
    install_requires=[
        "matplotlib",
        "numpy",
        "scipy",
        "scikit-image",
    ],
    setup_requires=[
        #"flake8",
    ],
    cmdclass={
        "coverage":CoverageCommand,
        #"test":TestCommandParallel,
    },
)
