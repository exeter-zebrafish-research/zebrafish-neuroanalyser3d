#
# AUTHOR      : J. Metz <metz.jp@gmail.com>
# DESCRIPTION : Test utility functions
#

import unittest
from unittest import mock
import zebrafish_neuroanalyser.utility as utility
import numpy as np
import random
import string

class Test_trunc(unittest.TestCase):
    def setUp(self):
        self.longstr = "".join((random.choice(string.ascii_letters)
            for i in range(1000)))
        self.length = 50
        self.lengthOdd = 51

    def test_trunc(self):
        res = utility.trunc(self.longstr, self.length)
        self.assertEqual(len(res), self.length)

    def test_trunc_odd_length(self):
        res = utility.trunc(self.longstr, self.lengthOdd)
        self.assertEqual(len(res), self.lengthOdd)


class Test_sort_filenames(unittest.TestCase):
    def setUp(self):
        self.nfiles = 100
        self.timestr = "\d{8}"
        self.filenames = [
            "%s_%s"%(
                "".join((random.choice(string.ascii_letters)
                for i in range(10))),
                "%0.8d.tif"%random.randint(0, 1000000),
            )
            for j in range(self.nfiles)
        ]

    def test_simple(self):
        res = utility.sort_filenames(self.filenames, self.timestr)
        self.assertEqual(len(res), self.nfiles)
        self.assertEqual(res, sorted(self.filenames, key=lambda f: f[10:]))
