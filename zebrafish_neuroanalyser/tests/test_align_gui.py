#
# AUTHOR      : J. Metz <metz.jp@gmail.com>
# DESCRIPTION : Test Align GUI functions
#

import unittest
from unittest import mock
import zebrafish_neuroanalyser.gui.align_labels as aligngui
from PyQt5.QtTest import QTest
from PyQt5.QtCore import Qt

app = aligngui.QtWidgets.QApplication([])

class Test_align_gui_application_window(unittest.TestCase):
    def test_basic_object(self):
        # Check that we create the correct object
        dialog = aligngui.ApplicationWindow()
        self.assertTrue(isinstance(dialog, aligngui.ApplicationWindow))

    def test_press_pick(self):
        # Check that we create the correct object
        dialog = aligngui.ApplicationWindow()
        dialog.canvas.toolbar.action_pick.trigger()

    def test_add_point(self):
        dialog = aligngui.ApplicationWindow()
        dialog.canvas.toolbar.action_pick.trigger()
        QTest.mouseClick(
            dialog.canvas,
            Qt.LeftButton,
            #pos=(100,100)
            )
