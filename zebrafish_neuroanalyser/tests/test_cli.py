#
# AUTHOR      : J. Metz <metz.jp@gmail.com>
# DESCRIPTION : Test the command line interface functions
#

import unittest
from unittest import mock
import zebrafish_neuroanalyser.cli as cli
import argparse
import logging


class Test_run_cli(unittest.TestCase):

    @mock.patch('zebrafish_neuroanalyser.gui.qt5.main_runner')
    @mock.patch('zebrafish_neuroanalyser.utility.logger.warning')
    def test_cli_basic_qt(self, mainfunc, logfunc):
        cli.run_cli(argv=[])


    @mock.patch('zebrafish_neuroanalyser.main.main')
    @mock.patch('zebrafish_neuroanalyser.utility.logger.warning')
    @mock.patch("zebrafish_neuroanalyser.utility.add_file_handler_to_global_logger")
    def test_cli_cli(self,
            mockfilelog,
            logfunc,
            mainfunc,
            ):
        cli.run_cli(argv=["--cli"])


    @mock.patch('zebrafish_neuroanalyser.main.main')
    @mock.patch('zebrafish_neuroanalyser.utility.logger.critical')
    @mock.patch('zebrafish_neuroanalyser.utility.logger.warning')
    @mock.patch("zebrafish_neuroanalyser.utility.add_file_handler_to_global_logger")
    def test_cli_cli_fail(self,
            mockfilelog,
            logfunc_warn,
            logfunc_critical,
            mainfunc,
            ):
        mainfunc.side_effect = Exception("MAIN FAIL")
        with self.assertRaises(Exception):
            cli.run_cli(argv=["--cli"])


    @mock.patch('os.symlink')
    @mock.patch('zebrafish_neuroanalyser.gui.align_labels.run_align_gui')
    @mock.patch('zebrafish_neuroanalyser.utility.logger.warning')
    @mock.patch('zebrafish_neuroanalyser.utility.add_file_handler_to_global_logger')
    def test_cli_align_gui(
            self,
            mockfilelog,
            mocklogfunc,
            mockmainfunc,
            mocksymlink,
            ):
        cli.run_cli(argv=["--aligngui"])

class Test_create_parser(unittest.TestCase):
    def test_create_parser(self):
        res = cli.create_parser()
        self.assertTrue(isinstance(res, argparse.ArgumentParser))
