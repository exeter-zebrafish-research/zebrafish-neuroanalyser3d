#
# AUTHOR      : J. Metz <metz.jp@gmail.com>
# DESCRIPTION : Test analysis functions
#

import unittest
from unittest import mock
import zebrafish_neuroanalyser.analysis as analysis
from zebrafish_neuroanalyser.tests.utility import random_string, do_nothing
import numpy as np
import io
import os

class Test_get_stats_on_rois(unittest.TestCase):
    def setUp(self):
        self.tmax  = 100
        self.shape = (self.tmax,10,50,50)
        self.nrois = 10
        self.randomrois = {
            i:np.random.rand(*self.shape[1:])>0.6
            for i in range(self.nrois)
        }
        self.emptyrois = {
            i:np.zeros(self.shape[1:], dtype=bool)
            for i in range(self.nrois)
        }
        self.data = np.random.rand(*self.shape)

    def test_empty_rois(self):
        stats = analysis.get_stats_on_rois(self.emptyrois, self.data)
        self.assertEqual(self.nrois, len(stats))
        self.assertEqual(
            sorted(self.randomrois.keys()),
            sorted(stats.keys())
        )
        self.assertEqual(
            stats,
            {
                i:{
                    "mean": [0 for t in range(self.tmax)],
                    "median": [0 for t in range(self.tmax)],
                    "stdv": [0 for t in range(self.tmax)],
                }
            for i in range(self.nrois)
            }
        )

    def test_random_rois(self):
        stats = analysis.get_stats_on_rois(self.randomrois, self.data)
        self.assertEqual(self.nrois, len(stats))
        self.assertEqual(
            sorted(self.randomrois.keys()),
            sorted(stats.keys())
        )
        self.assertEqual(
            stats,
            {
                i:{
                    "mean": [self.data[t][r].mean()
                        for t in range(self.tmax)],
                    "median": [np.median(self.data[t][r])
                        for t in range(self.tmax)],
                    "stdv": [self.data[t][r].std()
                        for t in range(self.tmax)],
                }
            for i,r in sorted(self.randomrois.items())
            }
        )



class Test_run_peak_analysis_on_cluster_data(unittest.TestCase):
    def setUp(self):
        self.tmax  = 100
        self.n_clusters = 10
        self.cluster_means_uniform = np.random.rand(self.n_clusters, self.tmax)
        self.scale_filt = 1.5
        self.output_path = random_string(50)
        self.output_types = ['png', 'pdf']

    @mock.patch("builtins.open")
    @mock.patch("matplotlib.pyplot.Figure.savefig")
    def test_uniform_random(self,
            mockopen,
            mocksavefig,
            ):
        mockopen.return_value = io.BytesIO()
        corrected = analysis.run_peak_analysis_on_cluster_data(
            self.cluster_means_uniform,
            self.output_path,
            self.output_types,
            update_function = do_nothing,
            scale_filt = self.scale_filt,
        )
        mockopen.assert_has_calls([
            mock.call(os.path.join(
                self.output_path,
                "cluster_analysis_minimum.%s"%ext))
            for ext in self.output_types])


class Test_run_peak_analysis_on_region_data(unittest.TestCase):
    def setUp(self):
        self.tmax  = 100
        self.n_regions = 10
        self.region_stats_uniform = {
            i: {
                "mean"  : np.random.rand(self.tmax),
                "median": np.random.rand(self.tmax),
                "stdv"  : np.std(np.random.rand(self.tmax, 100), axis=1),
            }
            for i in range(self.n_regions)
        }
        self.scale_filt = 1.5
        self.output_path = random_string(50)
        self.output_types = ['png', 'pdf']

    @mock.patch("builtins.open")
    @mock.patch("matplotlib.pyplot.Figure.savefig")
    def test_uniform_random(self,
            mockopen,
            mocksavefig,
            ):
        mockopen.return_value = io.BytesIO()
        corrected = analysis.run_peak_analysis_on_region_data(
            self.region_stats_uniform,
            self.output_path,
            self.output_types,
            update_function = do_nothing,
            scale_filt = self.scale_filt,
        )
        #print("\n".join(str(c) for c in mockopen.call_args_list))
        mockopen.assert_has_calls([
            mock.call(os.path.join(
                self.output_path,
                "labelled_analysis_minimum.%s"%ext))
            for ext in self.output_types])

