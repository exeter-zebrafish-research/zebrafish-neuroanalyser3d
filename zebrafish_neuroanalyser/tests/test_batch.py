from zebrafish_neuroanalyser import batch
from zebrafish_neuroanalyser.tests.utility import (
    random_string,
)
import unittest
from unittest import mock
import os
import random

class Test_generate_argslist(unittest.TestCase):
    def setUp(self):
        self.root    = random_string(50)
        self.runkwargs = {'arg1':'dummy', 'arg2':1}
        self.n_dirs = 100

    @mock.patch("os.listdir")
    @mock.patch("os.path.isdir")
    @mock.patch("glob.glob")
    @mock.patch("builtins.print")
    def test_simple(
            self,
            mockprint,
            mockglob,
            mockisdir,
            mocklistdir,
            ):
        mocklistdir.return_value = [
            random_string(50)
            for i in range(self.n_dirs)
        ]
        mockisdir.return_value = True
        def side_effect(args):
            return [os.path.join(
                os.path.dirname(args),
                "%sTL%0.6d.tif"%(random_string(10), random.randint(0,10000))
            ) for i in range(10)]
        mockglob.side_effect = side_effect
        args = batch.generate_arglists(
            self.root,
            **self.runkwargs,
        )
        self.assertEqual(len(args), self.n_dirs)


    def test_no_root(self):
        self.assertRaises(Exception, batch.generate_arglists, None)


    def test_generate_test_arglists_empty_in(self):
        batch.generate_test_arglists()
