#
# AUTHOR      : J. Metz <metz.jp@gmail.com>
# DESCRIPTION : Test the main workflow
#


import unittest
from unittest import mock
import zebrafish_neuroanalyser.main as zmain
import zebrafish_neuroanalyser.utility as utility
import logging
import numpy as np


class Test_Main(unittest.TestCase):
    datashape = (200,10,50,50)
    def setUp(self):
        # Setup function before each test method
        # Generate test data in-memory (use mock module)
        self.filenames = ["file1", "file2"]
        self.sample_factor=[3,3]

    def dummy_makedirs(dirname):
        return None

    def dummy_add_handler(*args, **kwargs):
        return

    def dummy_loader(*args, **kwargs):
        randomdata = np.random.rand(*Test_Main.datashape)
        return randomdata, Test_Main.datashape

    def dummy_roi_loader(*args, **kwargs):
        return np.array(
            [[10,10,10],
            [20,20,20],
            [30,30,30],
            [40,40,40]]
        )
    def dummy_get_rois(*args, **kwargs):
        s = Test_Main.datashape[1:]
        roi1 = np.zeros(s, dtype=bool)
        roi2 = np.zeros(s, dtype=bool)
        roi3 = np.zeros(s, dtype=bool)

        roi1[1*s[0]//5:2*s[0]//5,
             1*s[1]//5:2*s[1]//5,
             1*s[2]//5:2*s[2]//5] = 1
        roi2[3*s[0]//5:4*s[0]//5,
             3*s[1]//5:4*s[1]//5,
             3*s[2]//5:4*s[2]//5] = 1
        roi3[1*s[0]//5:2*s[0]//5,
             3*s[1]//5:4*s[1]//5,
             3*s[2]//5:4*s[2]//5] = 1

        roinames = {1:"a", 2:"b", 3:"c"}
        rois = { 1:roi1, 2:roi2, 3:roi3}
        return rois, roinames

    def dummy_plot_rois_on_max_proj(*args, **kwargs):
        return None

    def dummy_plot_data(*args, **kwargs):
        return None

    def dummy_output_images_and_excel_sheets_for_time_projected_data(
            *args,
            **kwargs
            ):
        return None

    def dummy_generate_labelled_regions_on_cluster_data_images(
            *args,
            **kwargs
            ):
        return None

    def dummy_do_nothing(*args, **kwargs):
        return None

    @mock.patch('os.makedirs')
    @mock.patch('os.remove')
    @mock.patch('zebrafish_neuroanalyser.plotting.savefig')
    @mock.patch('numpy.savez_compressed')
    @mock.patch('zebrafish_neuroanalyser.identify_clusters.analyze_label_cluster_relationships_and_save')
    def test_main(
            self,
            mockmakedirs,
            mockremove,
            mocksavefig,
            mocksavesz,
            mockclusterlabelfunc,
            ):
        utility.print = self.dummy_do_nothing
        zmain.logger.setLevel(logging.NOTSET)
        zmain.add_file_handler_to_global_logger = self.dummy_add_handler
        zmain.load_and_resample = self.dummy_loader
        zmain.load_points_from_file = self.dummy_roi_loader
        zmain.get_rois = self.dummy_get_rois
        zmain.plot_rois_on_max_proj = self.dummy_plot_rois_on_max_proj
        zmain.plot_data = self.dummy_plot_data
        zmain.output_images_and_excel_sheets_for_time_projected_data = \
            self.dummy_output_images_and_excel_sheets_for_time_projected_data
        zmain.generate_labelled_regions_on_cluster_data_images = \
            self.dummy_generate_labelled_regions_on_cluster_data_images
        zmain.run_peak_analysis_on_region_data = self.dummy_do_nothing
        zmain.run_peak_analysis_on_cluster_data = self.dummy_do_nothing
        zmain.plot_lines = self.dummy_do_nothing
        zmain.save_full_profiles_to_csv = self.dummy_do_nothing
        zmain.save_cluster_means_to_csv = self.dummy_do_nothing
        zmain.save_labelled_region_means_to_csv = self.dummy_do_nothing
        zmain.save_corrected_cluster_means_to_csv = self.dummy_do_nothing
        zmain.save_corrected_labelled_region_means_to_csv = self.dummy_do_nothing

        return_code = zmain.main(
            self.filenames,
            sample_factor=self.sample_factor,
            output_path="argy_bargy",
        )

        self.assertTrue(return_code)
