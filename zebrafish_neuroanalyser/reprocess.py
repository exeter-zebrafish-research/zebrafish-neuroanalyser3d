"""reprocess.py

Reprocess output files to generate new versions with modifications

J. Metz <metz.jp@gmail.com>
"""
import os
from zebrafish_neuroanalyser import (
    analysis,
    dataio,
    utility,
)
from zebrafish_neuroanalyser.gui import qt5 as gui


def run():
    """
    Run in reprocessing mode
    """

    reprocess_mean_to_median_peak_analysis()


def reprocess_mean_to_median_peak_analysis(output_folder=None):
    """
    Load the region stats for specified data-sets.
    If none are given, prompt user to select folder.
    Then reperform the peak analysis, but using the median
    region trace, instead of the mean.
    """

    if output_folder is None:
        output_folder = gui.get_existing_folder_gui_standalone(
            "Select root folder for reprocessing")

    region_trace_files = dataio.get_all_region_trace_files(
        output_folder, recursive=True)

    # For each trace file, generate the corresponding peak analysis file
    for tracefile in region_trace_files:
        run_and_save_peak_analysis_using_tracefile(tracefile)


def run_and_save_peak_analysis_using_tracefile(
        tracefile,
        output_types=['png'],
        update_function=utility.default_update_function,
        ):
    """
    Load the stats object from the given tracefile, run
    the peak analysis, and save the peak analysis result
    """
    output_path = os.path.dirname(tracefile)
    partial_stats, correctionname = dataio.load_stats_from_tracefile(tracefile)

    analysis.run_peak_analysis_on_corrected_stats_dict(
        partial_stats, output_path, output_types,
        update_function=update_function,
        name=correctionname)
