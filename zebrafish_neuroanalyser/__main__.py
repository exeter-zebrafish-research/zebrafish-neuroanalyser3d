#
# AUTHOR      : J. Metz <metz.jp@gmail.com>
# DESCRIPTION : CLI
#

from zebrafish_neuroanalyser.cli import run_cli
run_cli()
