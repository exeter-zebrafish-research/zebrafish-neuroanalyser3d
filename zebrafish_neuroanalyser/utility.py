#
# AUTHOR      : J. Metz <metz.jp@gmail.com>
# DESCRIPTION : Utility functions
#
import os
import re
import logging
import configparser


ROOT = os.path.dirname(os.path.dirname(__file__))
LOGROOT = os.path.join(ROOT, "logs")
if not os.path.isdir(LOGROOT):              # pragma: no cover
    os.makedirs(LOGROOT)


def trunc(s, length=50):
    eachside = (length - 3) // 2
    if (length - 3) % 2:
        middle = "...."
    else:
        middle = "..."
    return (
        "%s%s%s" % (s[:eachside], middle, s[-eachside:])
        if len(s) > length
        else s
    )


def sort_filenames(filenames, time_regex=None):
    if time_regex is None:
        return sorted(filenames)
    else:
        def keyfunc(fname):
            matches = re.findall(time_regex, os.path.basename(fname))
            if len(matches) == 1:
                return int(matches[0])
            else:
                return None
        logger.debug("Filenames:")
        logger.debug(str(filenames))
        return sorted(filenames, key=keyfunc)


def dummy_function(*args, **kwargs):        # pragma: no cover
    pass


def default_update_function(n=None, N=0, message=""):
    if n is None:
        print(message, flush="True")
    elif not message:
        print("[%d / %d]" % (n, N), end="\r", flush="True")
    else:
        print("[%d / %d] %s" % (n, N, message), end="\r", flush=True)


def default_error_function(err):            # pragma: no cover
    logger.error(err)


def complete(text, state):                  # pragma: no cover

    globres = glob.glob(os.path.expanduser(text)+'*')
    return (globres+[None])[state]


def default_get_filename_function():        # pragma: no cover
    readline.set_completer_delims(' \t\n;')
    readline.parse_and_bind("tab: complete")
    readline.set_completer(complete)
    filenames = input('Please enter the data file path: ').split(",")
    filenames = [f.strip() for f in filenames]
    if len(filenames) == 1:
        # Try a  glob
        filenames = sorted(glob.glob(filenames[0]))

    ntries = 0
    while not all([os.path.isfile(f) for f in filenames]):
        logger.warning(
            "Invalid file(s) selected - please try again (tries = %d)"
            % ntries)
        ntries += 1
        if ntries > 3:
            raise Exception(
                'Failed to enter a valid file path after 3 attempts')
        filenames = input('Please enter the data file path: ').split(",")
        filenames = [f.strip() for f in filenames]
    return filenames


# ------------------------------
# Create the logger
# ------------------------------
def create_logger():
    logger = logging.getLogger("zebrafish-analysis")
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    # create console handler with a high log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.WARNING)
    # create formatter and add it to the handlers
    ch.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    return logger


def add_file_handler_to_global_logger(output_path=ROOT, filename="run.log"):
    # Add a file logger
    formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh = logging.FileHandler(os.path.join(output_path, filename))
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    # return logger


def load_config(filename=None):
    if filename is None:
        filename = os.path.join(ROOT, "config.ini")
    config = configparser.ConfigParser()
    config.read(filename)
    return config


logger = create_logger()
