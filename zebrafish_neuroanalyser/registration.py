import numpy as np
from zebrafish_neuroanalyser.utility import (
    dummy_function,
)
import skimage.feature
from scipy.ndimage import fourier_shift

#------------------------------
# Frame alignment
#------------------------------

def align_frame(
        reference,
        frame,
        upsample_factor = None,
        update_function=dummy_function,
        clipping=True,   # NEW 2016/08/22 12:39:06 (BST)
        ):
    """
    Return new frame that is shifted according to reference
    As skimage doesn't support multidimensional (>2) resampling,
    have to do this part "manually"
    """
    if upsample_factor is not None:
        zoom = tuple([1 for n in range(frame.ndim-2)]+ [4,4])
        invzoom= ( 1.0/z for z in zoom)
        ref = ndi.zoom(reference, zoom)
        frm = ndi.zoom(frame, zoom)
    else:
        ref = reference
        frm = frame
    # Try clipping data to avoid mistake in registration due to large spike in
    # acitivity
    ref1 = ref.copy()
    frm1 = frm.copy()
    # Clip to same amount dictated by the reference frame
    med = np.median(ref)
    mad = np.median(np.abs(ref1 - med))
    clip = med + 10*mad
    ref1[ref1 > clip] = clip
    frm1[frm1 > clip] = clip

    shifts, error, phasediff = skimage.feature.register_translation(ref1, frm1)
    offset_image = fourier_shift(np.fft.fftn(frm), shifts)
    offset_image = np.fft.ifftn(offset_image)
    if upsample_factor is not None:
        return ndi.zoom(offset_image.real, invzoom)
    else:
        return offset_image.real


#------------------------------
# Point-set registration
#------------------------------


def _umeyama(src, dst, estimate_scale):
    """
    Copied from https://github.com/scikit-image/scikit-image/blob/master/skimage/transform/_geometric.py#L71
    Estimate N-D similarity transformation with or without scaling.
    Parameters
    ----------
    src : (M, N) array
        Source coordinates.
    dst : (M, N) array
        Destination coordinates.
    estimate_scale : bool
        Whether to estimate scaling factor.
    Returns
    -------
    T : (N + 1, N + 1)
        The homogeneous similarity transformation matrix. The matrix contains
        NaN values only if the problem is not well-conditioned.
    References
    ----------
    .. [1] "Least-squares estimation of transformation parameters between two
            point patterns", Shinji Umeyama, PAMI 1991, DOI: 10.1109/34.88573
    """

    num = src.shape[0]
    dim = src.shape[1]

    # Compute mean of src and dst.
    src_mean = src.mean(axis=0)
    dst_mean = dst.mean(axis=0)

    # Subtract mean from src and dst.
    # Eq (34) & (35)
    src_demean = src - src_mean
    dst_demean = dst - dst_mean

    # Eq. (38). (called $/Sigma_{xy}$)
    A = np.dot(dst_demean.T, src_demean) / num

    # Eq. (39). CALLED S
    d = np.ones((dim,), dtype=np.double)
    if np.linalg.det(A) < 0:
        d[dim - 1] = -1

    T = np.eye(dim + 1, dtype=np.double)

    U, S, V = np.linalg.svd(A)

    # Eq. (40) and (43).
    # R = U * S * V.T
    rank = np.linalg.matrix_rank(A)
    if rank == 0:
        return np.nan * T
    elif rank == dim - 1:       # Paper has rank >= m-1
        if (np.linalg.det(U) * np.linalg.det(V)) > 0:
            # As S = identity if det(U).det(V) = 1
            T[:dim, :dim] = np.dot(U, V.T)
        else:
            # Else S = diag(1,1,1,..,-1)
            s = d[dim - 1]
            d[dim - 1] = -1
            # i.e. T is R, d is S
            T[:dim, :dim] = np.dot(U, np.dot(np.diag(d), V.T))
            d[dim - 1] = s
    else:
        T[:dim, :dim] = np.dot(U, np.dot(np.diag(d), V.T))

    if estimate_scale:
        # Eq. (41) and (42).
        scale = 1.0 / src_demean.var(axis=0).sum() * np.dot(S, d)
    else:
        scale = 1.0

    T[:dim, dim] = dst_mean - scale * np.dot(T[:dim, :dim], src_mean.T)
    T[:dim, :dim] *= scale

    return T


def align_points(pts_ref, pts):
    mat_affine = np.diag(np.ones(4))
    matt2d = _umeyama(pts[:,1:], pts_ref[:,1:],
        estimate_scale=True,
    )
    mat_affine[1:,1:] = matt2d

    aff_z = _umeyama(pts[:,0][:,None], pts_ref[:,0][:,None],
        estimate_scale=True,
    )

    # Transform
    affine_scale_rotation = mat_affine[:3,:3]
    shift = mat_affine[:-1, -1]
    shift[0] = aff_z[0, -1]
    affine_scale_rotation[0,0] = aff_z[0,0]

    return affine_scale_rotation, shift


